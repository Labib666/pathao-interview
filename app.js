var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bearerToken = require('express-bearer-token');
var credVal = require('./middlewares/credValidator')();
var passport = require('./middlewares/passportAuth')();
passport.initialize();

var indexRouter = require('./routes/index');
var apiRouter = require('./routes/api');
var registerRouter = require('./routes/register');
var loginRouter = require('./routes/login');
var logoutRouter = require('./routes/logout');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routing done here
app.use('/', indexRouter);
app.use('/api', bearerToken(), credVal.validateJWT, passport.authenticate(), apiRouter);
app.use('/register', registerRouter);
app.use('/login', loginRouter);
app.use('/logout', logoutRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    'response': "error encountered",
    'error': res.locals.error
  });
});

module.exports = app;
