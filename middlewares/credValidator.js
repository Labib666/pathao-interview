var createError = require('http-errors');
var Token = require('../models/Token');
var jwtToken = require('jsonwebtoken');

/**
 * validates inputs for username, email and password 
 * provided by the user. more details can be added here.
 */

module.exports = function() {
    return {
        'validateUsername': function (req,res,next) {
            if(!req.body.username) {
                var err = createError(400);
                err.message = "invalid username";
                return next(err);
            }
            else {
                return next();
            }
        },
        'validateEmail': function (req,res,next) {
            if(!req.body.email) {
                var err = createError(400);
                err.message = "invalid email";
                return next(err);
            }
            else {
                return next();
            }
        },
        'validatePassword': function (req,res,next) {
            if(!req.body.password) {
                var err = createError(400);
                err.message = "invalid password";
                return next(err);
            }
            else {
                return next();
            }
        },
        'validateJWT': function(req,res,next) {
            if (!req.token) {
                var err = createError(400);
                err.message = "no jwt found";
                return next(err);
            }
            jwtToken.verify(req.token,process.env.JWT_SECRET,function(err,result){
                if (err){ 
                    return next(err);
                }
                Token.findOne({'token':req.token}, function(err,token){
                    if (err) {
                        return next(err);
                    }
                    if (!token) {
                        var err = createError(400);
                        err.message = "invalid jwt";
                        return next(err);
                    }
                    else {
                        return next();
                    }
                })
            });
        }
    };
}
