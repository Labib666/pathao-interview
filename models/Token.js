var mongoose = require('mongoose');  
var TokenSchema = new mongoose.Schema({  
    'token': String,
    'createdAt': { 
        'type': Date, 
        'expires': "600s",
        'default': Date.now 
    }
});
mongoose.model('Token', TokenSchema, 'tokens');
module.exports = mongoose.model('Token');