var express = require('express');
var router = express.Router();

/* GET request and logout endpoint */
router.get('/', function(req, res, next) {
    res.redirect('/api/logout');
});

module.exports = router;
