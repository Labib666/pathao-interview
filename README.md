# Pathao Interview

Create a RESTful API with the given specification

## Installation

- Download / Clone repository
- Copy .env.example to .env
- Make necessary change in .env ( Default is fine )
- If change made in port settings in .env, must make similar change in docker-compose.yml
- run `docker-compose up`

## Notes

- Base URL and PORT: 127.0.0.1:8080
- Mongodb Listens to PORT 27018
- JWT expires after 5 minutes. This can be modified.
- All JWT tokens must be sent via Authencation Header Bearer Token.
- Database Model: https://gitlab.com/Labib666/pathao-interview/wikis/database-model

## Usage

### GET /
- Says Hi if the API is Live

### POST /register
- registers new users
- expects 3 nonempty attributes in the body of http request
    - username
    - email
    - password
- username and email has to be unique
- in case of success, returns a JSON object with username and user ID in database

### POST /login
- logs in existing user
- expects 2 nonempty attributes in the body of http request
    - username
    - password
- fails if user does not exist or password mismatches
- in case of success, returns a JSON object. this object contains a JWT that must be submitted with every subsequent query to /api
- an entry is made for the token in database to identify that the user is logged in.
- the token expires after 5 minutes, and is also removed from the database 

### GET /logout

- logs out a logged in user
- user must be logged in (ie must send a valid JWT)
- the token entry is deleted from database

### GET /api

- accesses API
- user must be logged in (ie must send a valid JWT)
