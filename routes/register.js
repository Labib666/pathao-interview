var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var User = require('../models/User');
var credVal = require('../middlewares/credValidator')();

// List of middlewares to validate user input
var mwAra = [credVal.validateUsername,credVal.validateEmail,credVal.validatePassword];

/**
 *  POST request at register endpoint 
 *  Only lets validated requests through
 */
router.post('/', mwAra, function(req, res, next) {
    var username = req.body.username;
    var email = req.body.email;
    var password = bcrypt.hashSync(req.body.password,10);
    
    User.findOne({ $or:[ {'username': username}, {'email': email} ] }, function(err, user) {
        if (err) {
            return res.status(500).json({
                'response': "error processing request (db-find)"
            });
        }
        if (user) {
            // If username or email is taken
            return res.status(400).json({
                'response': "user or email already exists"
            });
        }
        else {
            // create new user
            User.create({
                'username': username,
                'email': email,
                'password': password
            }, function(err, newUser) {
                if (err) {
                    return res.status(500).json({
                        'response': "error processing request (db-create)"
                    });
                }
                else {
                    return res.status(200).json({
                        'response': "registration successfull",
                        'username': newUser.username,
                        '_id': newUser._id
                    });
                }
            });
        }
    });
});

module.exports = router;
