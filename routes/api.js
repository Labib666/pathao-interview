var express = require('express');
var router = express.Router();
var Token = require('../models/Token');

/* GET request at api */
router.get('/', function(req, res, next) {
    return res.status(200).json({
        'response': "user verified",
        'username': req.user.username,
        'user ID': req.user._id
    });
});

/* GET request at logout endpoint */
router.get('/logout', function(req, res, next) {
    Token.findOneAndRemove({'token':req.token}, function(err,token) {
        if (err) {
            return next(err);
        }
        else {
            return res.status(200).json({
                'response': "logout successful"
            });
        }
    });
});

module.exports = router;
