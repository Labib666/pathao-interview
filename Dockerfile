FROM node:8.11.1-alpine

RUN mkdir /pathao-interview

WORKDIR /pathao-interview

COPY package*.json ./

RUN npm install

COPY . .


