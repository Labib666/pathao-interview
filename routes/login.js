var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwtToken = require('jsonwebtoken');
var User = require('../models/User');
var Token = require('../models/Token');
var credVal = require('../middlewares/credValidator')();

// List of middlewares to validate user input
var mwAra = [credVal.validateUsername,credVal.validatePassword];

/**
 *  POST request at login endpoint 
 *  Only lets validated requests through
 */
router.post('/', function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    User.findOne({ 'username': username }, function(err,user) {
        if (err) {
            return res.status(500).json({
                'response': "error processing request (db-find)"
            });
        }
        if (!user) {
            return res.status(400).json({
                'response': "user does not exist"
            });
        }
        else {
            if (!bcrypt.compareSync(password,user.password)) {
                return res.status(400).json({
                    'response': "password does not match"
                });
            }
            else {
                var token = jwtToken.sign({
                    '_id': user._id,
                }, process.env.JWT_SECRET, { 'expiresIn': "5m" });
                // create new token
                // can be made more secure
                Token.create({
                    'token': token
                }, function(err,newToken) {
                    if (err) {
                        return res.status(500).json({
                            'response': "error processing request (db-create)"
                        });
                    }
                    else {
                        return res.status(200).json({
                            'response': "login successful",
                            'username': user.username,
                            '_id': user._id,
                            'token': token
                        });
                    }
                });
            }
        }
    });
});

module.exports = router;
